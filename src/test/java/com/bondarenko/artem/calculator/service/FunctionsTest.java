package com.bondarenko.artem.calculator.service;

import com.bondarenko.artem.calculator.domain.Function;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(JUnit4.class)
public class FunctionsTest {
    private FunctionsService functions = mock(FunctionsService.class);
    Function f = mock(Function.class);

    @Before
    public void setup() {
        functions.expressions = new HashMap<>() {{
            put("a+b;a,b", new Function("a+b;a,b"));
        }};
    }

    @Test
    public void calculateGoodParamsTest() {
        doCallRealMethod().when(functions).calculate(anyString());

        functions.calculate("a=10 b=11");

        Map<String, Double> params = new HashMap<>();
        params.put("a", 10.0);
        params.put("b", 11.0);
        verify(functions).calculateWithParams(params);
    }

    @Test
    public void calculateBadParamsTest() {
        doCallRealMethod().when(functions).calculate(anyString());

        functions.calculate("a=10 b=3");

        Map<String, Double> params = new HashMap<>();
        params.put("a", 10.0);
        params.put("b", 3.0);
        verify(functions).calculateWithParams(params);
    }

    @Test
    public void runCalculations() {
        doCallRealMethod().when(functions).calculateWithParams(anyMap());
        doReturn(true).when(f).canBeCalculated(anySet());

        String func = "a+b;a,b";
        functions.expressions.put(func, f);

        Map<String, Double> params = new HashMap<>();
        params.put("a", 10.0);
        params.put("b", 11.0);
        functions.calculateWithParams(params);

        verify(f).canBeCalculated(params.keySet());
        verify(f).calculate(params);
    }

    @Test
    public void functions() {
        doCallRealMethod().when(functions).functions();
        List<Function> funcs = functions.functions();
        assertThat(funcs.size(), is(1));
        assertThat(funcs.get(0).getExpression(), is("a+b"));
    }

    @Test
    public void addFunction() {
        doCallRealMethod().when(functions).addFunction(anyString());
        doCallRealMethod().when(functions).functions();
        functions.addFunction("a-b;a,b");
        assertThat(functions.functions().size(), is(2));
    }
}