package com.bondarenko.artem.calculator.domain;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(JUnit4.class)
public class FunctionTest {

    @Test
    public void calculate() {
        Map<String, Double> params = new HashMap<>() {{
            put("a", 1.0);
            put("b", 2.0);
        }};
        Function f = new Function("a+b;a,b");
        double result = f.calculate(params);
        assertThat(result, is(3.0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void calculateInvalid() {
        Map<String, Double> params = new HashMap<>() {{
            put("a", 1.0);
        }};
        Function f = new Function("a+b;a,b");
        double result = f.calculate(params);
    }

    @Test
    public void canBeCalculatedTrue() {
        Map<String, Double> params = new HashMap<>() {{
            put("a", 1.0);
            put("b", 2.0);
        }};
        Function f = new Function("a+b;a,b");
        assertThat(f.canBeCalculated(params.keySet()), is(true));
    }

    @Test
    public void canBeCalculatedFalse() {
        Map<String, Double> params = new HashMap<>() {{
            put("a", 1.0);
        }};
        Function f = new Function("a+b;a,b");
        assertThat(f.canBeCalculated(params.keySet()), is(false));
    }
}
