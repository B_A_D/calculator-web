package com.bondarenko.artem.calculator.domain;

import lombok.Getter;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import java.io.Serializable;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Getter
public class Function implements Serializable {

    private static final long serialVersionUID = -7751523688643835918L;
    private String expression;
    protected Expression exp;
    private Set<String> parameters = Collections.emptySet();

    public Function(String func) {
        init(func);
    }

    protected void init(String func) {
        String[] funcAndVars = func.split(";");
        expression = funcAndVars[0];
        ExpressionBuilder expressionBuilder = new ExpressionBuilder(expression);
        if (funcAndVars.length > 1)
            parameters = Stream.of(funcAndVars[1].split(",")).collect(Collectors.toSet());


        exp = expressionBuilder.variables(parameters).build();
    }

    public double calculate(Map<String, Double> parameters) {
        return exp.setVariables(parameters).evaluate();
    }

    public boolean canBeCalculated(Set<String> params) {
        return params.containsAll(parameters);
    }
}
