package com.bondarenko.artem.calculator.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Result {
    private String expression;
    private double value;
}
