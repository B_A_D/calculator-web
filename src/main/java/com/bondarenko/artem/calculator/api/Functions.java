package com.bondarenko.artem.calculator.api;

import com.bondarenko.artem.calculator.domain.Function;
import com.bondarenko.artem.calculator.domain.Result;

import java.util.List;
import java.util.Map;

public interface Functions {
    List<Result> calculate(String parameters);
    List<Result> calculateWithParams(final Map<String, Double> parameters);
    List<Function> functions();
    void addFunction(String func);
}
