package com.bondarenko.artem.calculator.service;

import com.bondarenko.artem.calculator.api.Functions;
import com.bondarenko.artem.calculator.domain.Function;
import com.bondarenko.artem.calculator.domain.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class FunctionsService implements Functions {
    @Value("${functions.file}")
    private String functionsFile;

    private static Logger logger = LoggerFactory.getLogger(FunctionsService.class);

    protected Map<String, Function> expressions = new HashMap<>();

    @PostConstruct
    public void loadFromFile() throws FileNotFoundException {
        logger.info("Load expressions from file {}", functionsFile);
        synchronized (expressions) {
            Map<String, Function> newExpressions = new HashMap<>();
            new BufferedReader(new InputStreamReader(new FileInputStream(functionsFile)))
                    .lines()
                    .forEach(l -> {
                        Function function = expressions.get(l);
                        newExpressions.put(l, function == null ? new Function(l) : function);
                    });
            expressions = newExpressions;
        }
    }

    @Override
    public List<Result> calculate(String parameters) {
        parameters = parameters.trim();
        Map<String, Double> params = new HashMap<>();
        String[] pparams = parameters.split(" ");
        for (String p :pparams) {
            String[] par = p.split("=");

            if (par.length != 2)
                logger.error("Invalid input: {}",p);
            else {
                try {
                    params.put(par[0], Double.parseDouble(par[1]));
                } catch (NumberFormatException e) {
                    logger.error("Error parsing '{}' as double", par[1]);
                }
            }
        }

        return calculateWithParams(params);
    }

    @Override
    public List<Result> calculateWithParams(final Map<String, Double> parameters) {
        logger.info("====================================");
        logger.info("Calculate with params {}", parameters);
        List<Result> result = new LinkedList<>();

        Set<String> params = parameters.keySet();
        synchronized (expressions) {
            expressions.keySet().stream().forEach(
                    k -> {
                        if (expressions.get(k).canBeCalculated(params)) {
                            double res = expressions.get(k).calculate(parameters);
                            logger.info("{}: {}", k, res);
                            result.add(new Result(expressions.get(k).getExpression(), res));
                        }
                        else logger.error("{}: value cant be calculated", k);
                    });
        }

        return result;
    }

    @Override
    public List<Function> functions() {
        return expressions.values().stream().collect(Collectors.toList());
    }

    @Override
    public void addFunction(String func) {
        expressions.put(func, new Function(func));
    }
}
