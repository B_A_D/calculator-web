package com.bondarenko.artem.calculator.controller;

import com.bondarenko.artem.calculator.domain.Function;
import com.bondarenko.artem.calculator.domain.Result;
import com.bondarenko.artem.calculator.service.FunctionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class Calculate {
    @Autowired
    private FunctionsService functionsService;

    @GetMapping("/calculate")
    public List<Result> calculate(@RequestBody Map<String, Double> params) {
        return functionsService.calculateWithParams(params);
    }

    @GetMapping("/functions")
    public List<Function> functions() {
        return functionsService.functions();
    }

    @PostMapping("/functions")
    public HttpStatus addFunction(@RequestBody String func) {
        functionsService.addFunction(func);
        return HttpStatus.CREATED;
    }
}
